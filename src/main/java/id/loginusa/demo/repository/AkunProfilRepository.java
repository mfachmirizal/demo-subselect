package id.loginusa.demo.repository;

import id.loginusa.demo.models.AkunProfil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AkunProfilRepository extends JpaRepository<AkunProfil, String> {

/*****
    Perhatian, Subselect tidak dapat di implimentasi menggunakan @Query seperti contoh dibawah,
 kita bisa menggunakan CriteriaBuilder untuk alternatifnya, contoh ini ada di Controllernya
 *******************************************************************/
//    @Query(value = "select " +
//            "a.akun_id AS akun_id, " +
//            "a.username AS username, " +
//            "p.nama AS nama, " +
//            "p.alamat AS alamat " +
//            "from akun a,profil p " +
//            "where a.akun_id = p.akun_id AND a.akun_id = :akunId")
//    AkunProfil getById(@Param("akunId") String akunId);

}

