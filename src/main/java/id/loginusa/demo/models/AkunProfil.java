package id.loginusa.demo.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Immutable
// query wajib di Aliaskan, dan di tempelkan ke field di bawah
@Subselect("select " +
        "a.akun_id AS akun_id, " +
        "a.username AS username, " +
        "p.nama AS nama, " +
        "p.alamat AS alamat " +
        "from akun a,profil p " +
        "where a.akun_id = p.akun_id")
public class AkunProfil {
    @Id
    @Column(name = "akunId")
    private String akunId;

    @Column(name = "username")
    private String username;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;
}
