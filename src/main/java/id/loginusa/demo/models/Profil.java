package id.loginusa.demo.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Data
@Table(name = "Profil")
public class Profil {
    @Id
    @Column(name = "profilId",length = 36, unique = true)
    private String profileId;

    @Column(name = "nama", length = 36)
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "usia")
    private int usia;

    @Column(name = "akunId")
    private String akunId;
}
