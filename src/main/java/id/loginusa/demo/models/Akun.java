package id.loginusa.demo.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Data
@Table(name = "Akun")
public class Akun {
    @Id
    @Column(name = "akunId",length = 36, unique = true)
    private String akunId;

    @Column(name = "username", length = 36)
    private String username;

    @Column(name = "password")
    private String password;
}
