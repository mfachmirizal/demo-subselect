package id.loginusa.demo.controllers;

import id.loginusa.demo.models.AkunProfil;
import id.loginusa.demo.models.Profil;
import id.loginusa.demo.repository.AkunProfilRepository;
import id.loginusa.demo.utils.MessageModelPagination;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@RestController
@RequestMapping("/akunprofil")
public class AkunProfilController {

    @Autowired
    EntityManager em;

    @Autowired
    private AkunProfilRepository akunProfilRepository;

    @GetMapping("/getall")
    public ResponseEntity<List<AkunProfil>> getAll(){
        //proses get ini adalah sample agar cepat dalam penulisan, silahkan pakai get all yg seperti biasanya
        List<AkunProfil> data = akunProfilRepository.findAll();
        try {
            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(data);
        }

    }

    @GetMapping("/getbyid/{akunId}")
    public ResponseEntity<AkunProfil> getById(@PathVariable("akunId") String akunId){
        try {
            //Define Class CriteriaBuilder
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<AkunProfil> cq = cb.createQuery(AkunProfil.class);

            //Define Where Clause
            Root<AkunProfil> akunProfilRoot = cq.from(AkunProfil.class);
            Predicate akunProfilPredicate = cb.equal(akunProfilRoot.get("akunId"), akunId); //akunId adalah column name yg di define di entity
            cq.where(akunProfilPredicate);

            //Pasangkan whereclause dengan Entity AkunProfil
            TypedQuery<AkunProfil> query = em.createQuery(cq);

            //dapatkan resultnya
            AkunProfil hasil = query.getSingleResult(); //getResultList = dapatkan List result, getSingleResult dapatkan hanya 1 Result

            return ResponseEntity.status(HttpStatus.OK).body(hasil);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new AkunProfil());
        }

    }

    @GetMapping("/searchprofil/{searchkey}")
    public ResponseEntity<List<Profil>> searchProfil(@PathVariable("searchkey") String searchkey){
        System.out.println("Search Key: "+ searchkey);
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Profil> cq = cb.createQuery(Profil.class);

            cq.distinct(true);

            Root<Profil> profilRoot = cq.from(Profil.class);

            Predicate predicateForData = cb.or(
//                    cb.like(cb.upper(profilRoot.get("profilId")), "%" + searchkey.toUpperCase() + "%"), //untuk id jangan ikut di search
                    cb.like(cb.upper(profilRoot.get("nama")), "%" + searchkey.toUpperCase() + "%"),
                    cb.like(cb.upper(profilRoot.get("alamat")), "%" + searchkey.toUpperCase() + "%"),
                    cb.like(cb.upper(profilRoot.get("usia").as(String.class)), "%" + searchkey.toUpperCase() + "%"),
                    cb.like(cb.upper(profilRoot.get("akunId")), "%" + searchkey.toUpperCase() + "%")
            );
            cq.where(predicateForData);

            TypedQuery<Profil> query = em.createQuery(cq);

            List<Profil> data = query.getResultList();

            return ResponseEntity.status(HttpStatus.OK).body(data);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ArrayList<Profil>());
        }

    }



}

/*Note
https://dev.to/pavankjadda/search-data-across-multiple-columns-using-spring-data-jpa-8ed
 */